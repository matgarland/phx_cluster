defmodule PhxClusterWeb.PageController do
  use PhxClusterWeb, :controller

  def index(conn, _params) do
    Node.connect(:b)
    Node.connect(:c)
    
    
    self_node = inspect(node())
    nodes = inspect(Node.list())
    render(conn, "index.html", %{node: self_node, nodes: nodes})
  end
end
