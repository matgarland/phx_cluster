# PhxCluster

This is an example project to demostrate node discovery with DNSPoll Strategy from libcluster.

There are 2 components needed.

1) Nginx that proxypasses requests to our app service.
2) Phoenix app that is scaled multiple times with docker-compose.

To start:

  * `docker-compose up --scale app=3`
  * Open [`http://localhost`](http://localhost) in your browser and see that the nodes are connected.

## Interesting configs

  * [`https://gitlab.com/egze/phx_cluster/blob/master/config/prod.exs#L58`](https://gitlab.com/egze/phx_cluster/blob/master/config/prod.exs#L58)
  * [`https://gitlab.com/egze/phx_cluster/blob/master/rel/env.sh.eex`](https://gitlab.com/egze/phx_cluster/blob/master/rel/env.sh.eex)
  * [`https://gitlab.com/egze/phx_cluster/blob/master/docker/prod/Dockerfile`](https://gitlab.com/egze/phx_cluster/blob/master/docker/prod/Dockerfile)
